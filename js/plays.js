/**
 * Created by chris on 5/11/15.
 */
var playsLoader =
{
    tz: jstz.determine(),

    init: function () {
            $.ajax({
                url: 'http://music.abcradio.net.au/api/v1/plays/search.json?station=triplej',
                dataType: 'json',
                //data: params,
                success: function (data) {
                    var result = [];

                    //Loop though items.
                    $(data.items).each(function () {
                        var eachItemRecording = this.recording;
                        var thisResult = {};

                        //Put the date back to local time for rendering.
                        thisResult.playedtime = this.played_time;

                        //Get Recording title
                        thisResult.title = eachItemRecording.title;

                        //Get the album art\
                        thisResult.albumimage = null;
                        if (eachItemRecording.releases.length) {
                            thisResult.albumimage = playsLoader.getImage(eachItemRecording, 100);
                        }

                        //Loop though artists to get details.
                        var featured = [];
                        $(eachItemRecording.artists).each(function () {
                            //Get main artist.
                            if (this.type == 'primary') {
                                thisResult.artistname = this.name;
                            } else if (this.type = 'featured') {
                                featured.push(this.name);
                            }
                        });

                        //Add featured artists to track title
                        if (featured.length) {
                            thisResult.title += " {Ft. " + featured.join(", ") + "}";
                        }

                        //Add track metadata to track title
                        if (eachItemRecording.metadata) {
                            thisResult.title += " (" + eachItemRecording.metadata + ")";
                        }

                        result.push(thisResult);
                    });

                    console.log(result);
                    playsLoader.renderTracks(result);
                },

                error: function () {
                    alert("Could not load Triple J playlist. Please try again later");
                }
            });
    },

    loadFavs: function(){
        var data = [];
        for (var i = 0; i < localStorage.length; i++){
            data.push(JSON.parse(localStorage.getItem(localStorage.key(i))));
        }
        var result = [];

        //Loop though items.
        $(data).each(function () {
            var thisResult = {};


            //Get Recording title
            thisResult.title = this.title;

            //Get the album art\
            thisResult.albumimage = null;
            if (this.albumimage) {
                thisResult.albumimage = this.albumimage;
            }

            //Loop though artists to get details.
            thisResult.artistname = this.artistname;

            result.push(thisResult);
        });

        console.log(result)
        if (!result.length)
            $('#emptyDiv').show();
        else
            $('#emptyDiv').hide();
        playsLoader.renderTracks(result);
    },


    getImage: function (track, width) {
        var image;
        // Try recording artwork
        if (track.artwork.length) {
            image = getResizedImage(track, width);
        }

        // Try release image
        if (!image && track.releases.length) {
            $.each(track.releases, function (i, release) {
                image = playsLoader.getResizedImage(release, width);
                if (image) {
                    return false;
                }

                // If original image is a sensible size then use it
                $.each(release.artwork, function () {
                    if (this.width <= (width * 2)) {
                        image = this.url;
                        return false;
                    }
                });
            });
        }

        // Try artist artwork
        if (!image && track.artists.length) {
            $.each(track.artists, function (i, artist) {
                if (artist.type == "primary") {
                    image = playsLoader.getResizedImage(artist, width);
                }
            });
        }

        return image;
    },

    getResizedImage: function (data, width) {
        var image = null;
        if (data.artwork) {
            $.each(data.artwork, function (i, artwork) {
                if (artwork.sizes) {
                    $.each(artwork.sizes, function () {
                        if (this.aspect_ratio == "1x1" && this.width == width) {
                            image = this.url;
                            return false;
                        }
                    });
                }
                if (image) {
                    return false;
                }
            });
        }
        return image;
    },

    removeBracketedChars: function(s) {
        var index = s.indexOf('{');
        if (index > 0) {
            return s.slice(0, index - 1);
        } else {
            return s;
        }
    },

    renderTracks: function(data)
    {

        $('#loadingDiv').hide();
        playsLoader.currentDate = null;

        var $results = $('#plays #results>ul');
        if (!data) {
            $("#error").html("<h2>Unable to retrieve Triple J broadcast data. Try again later.</h2>");
        }
        else
        {

            var time, date, timestamp;
            $.each(data, function()
            {

                if(this.playedtime !== playsLoader.lastPlayedTime || $('#btn-favs').hasClass('active'))
                {
                    timestamp = moment.utc(this.playedtime);
                    date = timestamp.tz(playsLoader.tz.name()).format('YYYY-MM-DD');
                    time = timestamp.tz(playsLoader.tz.name()).format('HH:mma');

                    //Create a break in the list for different days.
                    if (date != playsLoader.currentDate && !$('#btn-favs').hasClass('active'))
                    {
                        var $break = $('<li/>').addClass('break');
                        var $date = $('<span/>').addClass('date').text(date);
                        $break.append($date);
                        $results.append($break);
                        playsLoader.currentDate = date;
                    }

                    var $li;
                    if (!$('#btn-favs').hasClass('active'))
                        $li = $('<li/>').addClass('play');
                    else
                        $li = $('<li/>').addClass('favi');

                    var $time;

                    if (!$('#btn-favs').hasClass('active'))
                        $time = $('<p/>').addClass('time').text(time);
                    else
                        $time = "<br />";
                    var img = this.albumimage;

                    //If can't find image use default placeholder.
                    if (!img)
                    {
                        img = 'http://www.abc.net.au/triplej/albums/default/covers/100.jpg';
                    }

                    //Create album row.
                    var $img = $('<img/>').addClass('album-art').attr({
                        src: img,
                        alt: ''
                    }).error(function() {
                        $(this).unbind("error").attr('src', 'http://www.abc.net.au/triplej/albums/default/covers/100.jpg');
                    });

                    var $fav;
                    if (localStorage[this.artistname+this.title])
                        $fav = $('<div class="fav" data-image="'+ this.albumimage +'" data-artist="' + this.artistname + '" data-title="' + this.title + '"/>').addClass('filled');
                    else
                        $fav = $('<div class="fav" data-image="'+ this.albumimage +'" data-artist="' + this.artistname + '" data-title="' + this.title + '"/>').addClass('hollow');
                    var $artist = $('<p/>').addClass('artist').text(this.artistname);
                    var $track = $('<p/>').addClass('track').text(this.title);
                    var $search = $('<p/>').addClass('search');
                    var searchString = encodeURIComponent(this.artistname + " " + this.title);
                    var $yt = $('<a/>').text('YouTube').attr('target', '_blank').attr('href', 'http://www.youtube.com/results?search_query=' + searchString);
                    var $spotify = $('<a/>').text('Spotify').attr('target', '_blank').attr('href', 'http://open.spotify.com/search/artist:' + encodeURIComponent(this.artistname) + "%20track:" + encodeURIComponent(playsLoader.removeBracketedChars(this.title)));
                    var $rdio = $('<a/>').text('Rdio').attr('target', '_blank').attr('href', 'http://www.rdio.com/search/' + encodeURIComponent(this.artistname) + " " + encodeURIComponent(playsLoader.removeBracketedChars(this.title)));
                    var $google = $('<a/>').text('Google').attr('target', '_blank').attr('href', 'http://www.google.com/search?q=' + searchString);

                    //Append the result.
                    $search.append($yt, " | ", $spotify, " | ", $rdio, " | ", $google);
                    $li.append($img, $time, $track, $artist, $search, $fav);
                    $results.append($li);
                }

                playsLoader.lastPlayedTime = this.playedtime;
            });

        }
        $('.fav').click(function(evt){
            var song;
            song = {
                'title': $(evt.target).data('title'),
                'artistname': $(evt.target).data('artist'),
                'albumimage': $(evt.target).data('image')
            };

            if ($(evt.target).hasClass('filled')) {
                $(evt.target).removeClass('filled').addClass('hollow');
                localStorage.removeItem($(evt.target).data('artist')+$(evt.target).data('title'));
            }
            else {
                $(evt.target).removeClass('hollow').addClass('filled');
                localStorage.setItem($(evt.target).data('artist')+$(evt.target).data('title'), JSON.stringify(song));
            }
        });
    }
};
