//Triple J API URL - http://music.abcradio.net.au/api/v1/plays/search.json?station=triplej

window.addEventListener('load',function(){
	$('#btn-pl').addClass('active');

	playsLoader.init();

	$('#btn-pl').click(function(){
		$('.play, .break').remove();
		$(".play, .break").show();
		$(".favi").hide();
		$('#emptyDiv').hide();
		$(this).addClass('active');
		$('#btn-favs').removeClass('active');
		$('.heading').text("Playing on Triple J");
		playsLoader.init();
	});

	$('#btn-favs').click(function(){
		$('.favi').remove();
		$(".play, .break").hide();
		$(".favi").show();
		$(this).addClass('active');
		$('#btn-pl').removeClass('active');
		$('.heading').text("Favourited Songs");
		playsLoader.loadFavs();
	});

});



